import pika

topic = 'realtime'

credentials = pika.PlainCredentials(username='guest', password='guest')

parameters =  pika.ConnectionParameters('localhost', credentials=credentials, heartbeat=5)
connection = pika.BlockingConnection(parameters)

# connection = pika.BlockingConnection(pika.ConnectionParameters(host='127.0.0.1',port=5672,credentials=credentials,heartbeat=5))

channel = connection.channel()
channel.queue_declare(queue = topic)

def send(message):
    channel.basic_publish(exchange='', routing_key=topic, body=message)

