import twstock
import requests
from bs4 import BeautifulSoup as BS
import pandas as pd

def mon_float(input_ele):
    if input_ele == '--':
        return None
    if input_ele!= '--':
        return float(''.join(input_ele.split(',')))


def update_codes():
    twstock.__update_codes()
    print("update codes done.")


def get_realtime(code):
    # print(type(code))
    # print(code)
    stock = twstock.realtime.get(code[0].split(','))
    # print(stock)
    return stock

def get_info(code, prop):
    stock = twstock.Stock(code[0])
    # print(type(getattr(stock,prop[0])))
    return getattr(stock,prop[0])

def get_strategy(code):
    stock = twstock.Stock(code[0])
    bfp = twstock.BestFourPoint(stock)
    return bfp.best_four_point()

def TWSEquote(session):
    if session == 'regular':
        urlfut = 'http://info512.taifex.com.tw/EN/FusaQuote_Norl.aspx'
    elif session == 'afterhour':
        urlfut = 'http://info512ah.taifex.com.tw/EN/FusaQuote_Norl.aspx'

    resfut = requests.get(urlfut)
    resfut.encoding = 'utf-8'
    soup = BS(resfut.text,"lxml")
    table = pd.read_html(str(soup.select('#divDG')[0]),index_col=0,header=0)[0]
    divdata = table.iloc[0:].transpose().loc[['Status','Bid','Bid_V','Ask','Ask_V','Price','Change','Change%','Vol','Open','High','Low','Sett.','Time']].applymap(lambda x : str(x)) 
    return divdata.to_json()


def getStr(x):
    rtnStr = str(x)
    # print(rtnStr)
    if rtnStr == '公司代號' or rtnStr == '公司簡稱' or rtnStr == '營利事業統一編號' or rtnStr == '實收資本額(元)':
        pass
    else:
        return rtnStr
    

def CompanyProfile(market):

    if market == 'TWSE':
        url = 'https://mops.twse.com.tw/mops/web/ajax_t51sb01?encodeURIComponent=1&step=1&firstin=1&TYPEK=sii&code'
    elif market == 'TPEX':
        url = 'https://mops.twse.com.tw/mops/web/ajax_t51sb01?encodeURIComponent=1&step=1&firstin=1&TYPEK=otc&code'
    else:
        pass

    resfut = requests.get(url)
    resfut.encoding = 'utf-8'
    soup = BS(resfut.text,"lxml")
    table = pd.read_html(str(soup.select('table')))[0]
    divdata = table.iloc[0:].transpose().loc[['公司代號','公司簡稱','營利事業統一編號','實收資本額(元)']].applymap(lambda x: getStr(x)) 
    
    # print(type(divdata))
    # print(divdata)

    # print(divdata.dropna(axis=1))

    return divdata.dropna(axis=1).to_json()

def get_history(code):
    # stock = twstock.Stock(code[0])
    stock = twstock.Stock(code)
    # stock = get_info(['2330'],['data'])

    print(stock.fetch(2019,6))
    # print(stock.fetch_from(2019,6))
    # print(stock.fetch_31())
    # print(stock)

# print(get_realtime(['2492']))
# get_history('2330')

# print(
# CompanyProfile('TWSE')
# )
# print(CompanyProfile('TPEX'))

