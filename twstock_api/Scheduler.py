import schedule
import time
import datetime
import StockService
import redis
import Producer
import Publisher
import json
import twstock

def filterOut(stockCodeInfo):
    return stockCodeInfo.type == '股票'

keys=list(twstock.codes.keys())
values=list(filter(filterOut, twstock.codes.values())) 

seperator = ','
limit = 150

all_code = list(map(lambda x: x[1], values))

r = redis.Redis(host='127.0.0.1', port=6379)
p = Publisher.Publisher('localhost','/','guest','guest')

def switchOn():
    day = datetime.datetime.today().weekday()
    if day != 5 and day != 6:        
        r.set('switch:scheduler','on')

def switchOff():
    r.set('switch:scheduler','off')

def fetch():
    if bool(r.get('switch:scheduler').decode('utf-8') == 'on'):
        start = 0
        end = start + limit
        while end <= len(all_code) and start <= len(all_code):
            stock = StockService.get_realtime([seperator.join(all_code[start:end])])

            realTime_info = stock.values()
            for info in realTime_info:
                stockStr = json.dumps(info).encode('utf8').decode('utf8')    
                # Producer.send(stockStr)
                p.publish(stockStr)
            start = start + limit
            end = end + limit 
            if end > len(all_code):
                end = len(all_code)


# schedule.every(10).minutes.do(job)
# schedule.every().hour.do(job)
# schedule.every().day.at("10:30").do(job)
# schedule.every(10).seconds.do(job,schedule)
schedule.every(5).seconds.do(fetch)
schedule.every().day.at("08:25").do(switchOn)
schedule.every().day.at("14:30").do(switchOff)


while 1:
    schedule.run_pending()
    time.sleep(1)