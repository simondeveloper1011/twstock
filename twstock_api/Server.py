import sys
import http.server
import StockService
import urllib.parse as urlparse
import json

def obj_dict(obj):
    return str(obj)

def dispatcher(url):
    print("incoming url: %s" % (url))
    
    components = urlparse.urlparse(url)
    component = components.path.split('/')[1]

    if component == 'info':
        params = urlparse.parse_qs(components.query)
        info = StockService.get_info(params['code'], params['prop'])
        return json.dumps(info, default=obj_dict).encode('utf8')
    elif component == 'realtime':
        stock = StockService.get_realtime(urlparse.parse_qs(components.query)['code'])
        return json.dumps(stock, default=obj_dict).encode('utf8')
    elif component == 'hist':
        return "TBD"
    elif component == 'strategy':
        resp = StockService.get_strategy(urlparse.parse_qs(components.query)['code'])
        return json.dumps(resp, default=obj_dict).encode('utf8')
    elif component == 'update':
        StockService.update_codes()
        return "Update done."
    elif component == 'profiles':
        params = urlparse.parse_qs(components.query)
        resp = StockService.CompanyProfile(params['market'][0]).encode('utf8')
        # print(type(resp))
        return resp
    elif component == 'index':
        params = urlparse.parse_qs(components.query)
        resp = StockService.TWSEquote(params['session'][0]).encode('utf8')
        # print(type(resp))
        return resp
    else:
        return "Unknown path: %s" % (url)

class RequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        resp = dispatcher(self.path)
        self.send_response(200)
        self.send_header("Content-Type",'application/json; charset=\"utf-8\"')
        # self.send_header("Content-Length",len(str(resp)))
        self.end_headers()

        self.wfile.write(resp)

if __name__ == '__main__':
    # StockService.__update_codes()
    serverAddress = ('', int(sys.argv[1]))
    server = http.server.HTTPServer(serverAddress, RequestHandler)
    server.serve_forever()



    

