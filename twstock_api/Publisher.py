import logging
import json
import pika

class Publisher:
    EXCHANGE=''
    TYPE='realtime'
    ROUTING_KEY = 'realtime'


    def __init__(self, host, virtual_host, username, password):
        self._params = pika.connection.ConnectionParameters(
            host=host,
            virtual_host=virtual_host,
            credentials=pika.credentials.PlainCredentials(username,password),
            heartbeat=5)

        self._conn = pika.BlockingConnection(self._params)
        self._channel = self._conn.channel()
        self._channel.queue_declare(queue = self.TYPE)


    def connect(self):
        if not self._conn or self._conn.is_closed:
            self._conn = pika.BlockingConnection(self._params)
            self._channel = self._conn.channel()
            self._channel.queue_declare(queue = self.TYPE)


    def _publish(self, msg):
        self._channel.basic_publish(exchange=self.EXCHANGE,routing_key=self.ROUTING_KEY,body=msg)
        logging.debug('message sent: %s', msg)

    def publish(self, msg):
        """Publish msg, reconnecting if necessary."""

        try:
            self._publish(msg)
        except pika.exceptions.ConnectionClosed:
            logging.debug('reconnecting to queue')
            self.connect()
            self._publish(msg)
        except pika.exceptions.StreamLostError:
            self.connect()
            self._publish(msg)

    def close(self):
        if self._conn and self._conn.is_open:
            logging.debug('closing queue connection')
            self._conn.close()

print('starting...')
p=Publisher('localhost','/','guest','guest')
p.publish('test_msg')