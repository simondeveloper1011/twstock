FROM python:3

WORKDIR /app
ADD ./twstock_api /app/twstock_api
ADD ./twstock /app/twstock
ADD ./pyproject.toml ./flit.ini ./scripts/start.sh /app/
# ADD twstock_api/Server.py twstock_api/StockService.py /app/

RUN ls /app

RUN python3 -m pip install --user flit

ENV PATH $PATH:/root/.local/bin
ENV FLIT_ROOT_INSTALL 1

RUN flit install 

RUN pip3 install lxml
# RUN pip3 install twstock
RUN pip3 install beautifulsoup4

RUN pip3 install nose
RUN pip3 install tornado

RUN pip3 install pandas
RUN pip3 install html5lib
RUN pip3 install BeautifulSoup4

# scheduler lib
RUN pip3 install schedule
# rabbitmq lib
RUN pip3 install pika
# redis
RUN pip3 install redis

# RUN pip install jpserve


# update in runtime
# RUN twstock -U

EXPOSE 80

CMD ["/app/start.sh"]


